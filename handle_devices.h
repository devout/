#ifndef HANDLE_DEVICES_H
#define HANDLE_DEVICES_H

void show_device_options(void);

void print_one_event(unsigned long long event_num);

void print_multiple_events(int num_events, unsigned long long event_nums[]);

#endif
